sat = load('sat.trn');
labels = sat(:, end);
features = sat(:, 1:end-1);
class_one = labels == 1;
class_two = labels == 2;
features = [features(class_one, :);
            features(class_two, :)];
labels = [ones(sum(class_one), 1);
          2*ones(sum(class_two), 1)];

shuffling = randperm(size(labels, 1));
labels = labels(shuffling, :);
features = features(shuffling, :);

save('sat.features.mat', 'features')
save('satlabels.mat', 'labels')
