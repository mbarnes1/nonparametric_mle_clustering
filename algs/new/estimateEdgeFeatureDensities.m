function [ P ] = estimateEdgeFeatureDensities( X )
%ESTIMATEEDGEFEATUREDENSITIES Estimates the edge feature densities P(X)
%   Inputs
%       X - matrix of n m-dimensional features [n x m]
%	Outputs
%       P - probability density function fit to X

pd = fitdist(X, 'Kernel', 'Support', 'Positive')

end

