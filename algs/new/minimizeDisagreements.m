function [ clusters ] = minimizeDisagreements( E, C, varargin )
%MINIMIZEDISAGREEMENTS Correlation clustering on unweighted graphs, as
%published by Demaine et al. 2006
%   Inputs:
%       E -- edge binary adjacency matrix {0, 1}^[n x n]
%       C -- mistake cost adjacency matrix [n x n]. C \geq 0, C(i, i) = 0
%       (optional) 'Solver' -- 'Matlab' (default) or 'Cplex' (if available)
%   Outputs:
%       clusters -- clusters labels. [n x 1] column vector, labels are zero
%       indexed

%% Input checking
validateattributes(E, {'logical'}, {'square'})
validateattributes(C, {'numeric'}, {'nonnegative', 'square'})
if any(diag(C) ~= 0)
    error('Self edges should not have cost')
elseif length(E) ~= length(C)
    error('Matrix dimensions must match')
else
    n = length(E);
end
p = inputParser;
defaultSolver = 'Matlab';
expectedSolvers = {'Matlab', 'Cplex'};
addParameter(p,'Solver',defaultSolver,...
                 @(x) any(validatestring(x,expectedSolvers)));
parse(p, varargin{:});
solver = p.Results.Solver


%% LP
nvariables = (n^2-n)/2;  % one variable for every edge (undirected, no self edges)
ninequalities = nvariables*(n-2);  % one inequality for every pair of connecting edges
nequalities = 0;  % no equalities. One variable per undirected edge

f = C.*(2*E-1);
f = squareform(f, 'tovector');
sub_to_f = squareform(1:length(f));
A = NaN(ninequalities*3, 3);
b = sparse(ninequalities, 1);
count = 1;
sparse_count = 1;
disp('Constructing LP inequalities...'); tic;
for u = 1:(n-1)
    for v = (u+1):n  % Loop through all variables
        w = true(n,1);
        w(u) = 0;
        w(v) = 0;
        iXvw = sub_to_f(v, w)';
        iXuw = sub_to_f(u, w)';
        iXuv = sub_to_f(u, v)*ones(size(iXuw));
        if ~isequal(size(iXuv), size(iXvw), size(iXuw))
            error('Index sizes should be the same')
        elseif ~isempty(iXuv)
            A(sparse_count:(sparse_count + length(iXuv)-1), :) = [(count:(count+length(iXuv)-1))', iXuv, -1*ones(size(iXuv))];
            sparse_count = sparse_count + length(iXuv);
            A(sparse_count:(sparse_count + length(iXvw)-1), :) = [(count:(count+length(iXvw)-1))', iXvw, -1*ones(size(iXvw))];
            sparse_count = sparse_count + length(iXvw);
            A(sparse_count:(sparse_count + length(iXuw)-1), :) = [(count:(count+length(iXuw)-1))', iXuw, ones(size(iXuw))];
            sparse_count = sparse_count + length(iXuw);
            count = count+length(iXuw);
        end
    end
end
A = sparse(A(:,1), A(:,2), A(:,3), ninequalities, nvariables); toc;
lb = zeros(nvariables, 1);
ub = ones(nvariables, 1);
disp(['Solving LP with ', num2str(nvariables), ' variables, ', ...
    num2str(ninequalities), ' inequalities, and ', num2str(nequalities), ...
    ' equalities']); tic;
if strcmp(solver, 'Matlab')
    options = optimoptions('linprog','Display','Iter','Algorithm','dual-simplex');  % dual-simplex is fastest solver 
    [X,~,~,~] = linprog(f, A, b, [], [], lb, ub, [], options);
elseif strcmp(solver, 'Cplex')
    addpath('~/Applications/IBM/ILOG/CPLEX_Studio1263/cplex/matlab/x86-64_osx')
    options = cplexoptimset('Display', 'iter', 'Algorithm', 'auto');
    [X,~,~,~] = cplexlp(f, A, b, [], [], lb, ub, options);
end
toc;
X(X<0 & X>-1E-8) = 0;
X = squareform(X);
if ~issymmetric(X)
    error('Solution should be symmetric')
end
X(logical(eye(size(X)))) = Inf;

%% Region growing (i.e. LP-rounding)
remaining_nodes = true(n, 1);
clusters = NaN(n, n);
i = 1;
I = vol(true(n,1), Inf, E, C, X)/n;  % initial volume for balls
c = 1/log(n+1) + 2;
X_used = X;
while any(remaining_nodes)
    cluster = false(n, 1);
    node1 = datasample(1:n, 1, 'Weights', remaining_nodes);
    if remaining_nodes(node1)  % remove this error check later
        remaining_nodes(node1) = 0;
    else
        error('Sampled already clustered node')
    end
    cluster(node1) = 1;
    j = 0;
    r = 0;
    LP_distances = X_used(node1, :);
    [LP_distances, id] = sort(LP_distances, 'ascend');
    empty = false;
    while cut(cluster, E, C) > c*log(n+1)*vol(cluster, r, E, C, X, I)
        j = j+1;
        if LP_distances(j) == Inf
            empty = true;
            break
        end
        if ~remaining_nodes(id(j))  % remove this error check alter
            error('Sampled already clustered node')
        end
        cluster(id(j)) = 1;
        r = LP_distances(j);
        disp(['LP region-growing: ', num2str((n-sum(remaining_nodes)+sum(cluster))/n*100), '%'])
    end
    if ~empty && j~=0
        cluster(id(j)) = 0;
        empty = false;
    end
    remaining_nodes(cluster) = 0;
    X_used(cluster, :) = Inf;
    X_used(:, cluster) = Inf;
    clusters(:, i) = cluster;
    disp(['Cluster ', num2str(i), ' created'])
    i = i+1;
end
clusters = clusters(:,all(~isnan(clusters)));  % remove pre-allocated columns
if ~all(sum(clusters, 2) == ones(n,1))
    error('Each node must be assigned to exactly one cluster')
end
k = size(clusters, 2);
clusters = clusters*(1:k)';
end


function [ out ] = cut(S, E, C)
%CUT The cut of a set of vertices (Demaine et al.)
%   Inputs:
%       S -- logical vector of nodes within the cut
%       E -- edge binary adjacency matrix {logical}^[n x n]
%       C -- mistake cost adjacency matrix [n x n]. C \geq 0, C(i, i) = 0
%   Outputs:
%       out -- value of the cut
validateattributes(S, {'logical'}, {'vector'})
validateattributes(E, {'logical'}, {'square'})
validateattributes(C, {'numeric'}, {'nonnegative', 'square'})
if any(diag(C) ~= 0)
    error('Self edges should not have cost')
elseif ~isequal(length(S), length(E), length(C))
    error('Matrix dimensions must match')
end

radiating_edges = bsxfun(@and, S(:), ~S(:)');
radiating_edges = radiating_edges | radiating_edges';
out = sum(sum(C(radiating_edges & E)));
validateattributes(out, {'numeric'}, {'scalar', '>=', 0});
end


function [ out ] = vol(S, r, E, C, X, varargin)
%VOL The volume of a set of vertices (Demaine et al.)
%   Inputs:
%       S -- logical vector of nodes within the cut
%       r -- radius of the ball
%       E -- edge binary adjacency matrix {logical}^[n x n]
%       C -- mistake cost adjacency matrix [n x n]. C \geq 0, C(i, i) = 0
%       X -- solution to the linear program
%       (optiona) I -- the initial weight of the volume (default = 0)
%   Outputs:
%       out -- value of the volume
validateattributes(S, {'logical'}, {'vector'})
validateattributes(r, {'numeric'}, {'scalar', '>=', 0})
validateattributes(E, {'logical'}, {'square'})
validateattributes(C, {'numeric'}, {'nonnegative', 'square'})
validateattributes(X, {'numeric'}, {'nonnegative', 'square'})
if any(diag(C) ~= 0)
    error('Self edges should not have cost')
elseif ~isequal(length(S), length(E), length(C), length(X))
    error('Matrix dimensions must match')
end
if length(varargin) == 1
    I = varargin{1};
    validateattributes(I, {'numeric'}, {'scalar', '>=', 0})
else
    I = 0;
end

inner_edges = bsxfun(@and, S(:), S(:)');
inner_edges = inner_edges | inner_edges';
inner_edges(logical(eye(size(inner_edges)))) = 0;
radiating_edges = bsxfun(@and, S(:), ~S(:)');
radiating_edges = radiating_edges | radiating_edges';
radiating_edges(logical(eye(size(radiating_edges)))) = 0;

weighted_edges = mymmult(C, X);
fractionals = r-X;  % Demaine says r-X. Is this a typo?
fractionals(isnan(fractionals)) = Inf;
%validateattributes(fractionals, {'numeric'}, {'nonnegative'})
fractionally_weighted_edges = mymmult(C, fractionals);
out = I + sum(sum(weighted_edges(inner_edges & E))) + sum(sum(fractionally_weighted_edges(radiating_edges & E)));
%validateattributes(out, {'numeric'}, {'scalar', '>=', 0});
end

function [ Z ] = mymmult(X, Y)
%MYMMULT X.*Y where 0*Inf = 0
validateattributes(X, {'numeric'}, {'nonnan'})
validateattributes(Y, {'numeric'}, {'nonnan'})
Z = X.*Y;
Z(isnan(Z)) = 0;
end
