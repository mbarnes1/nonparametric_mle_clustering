function [ Y ] = randsampleMultivariate( population, k )
%RANDSAMPLEMULTIVARIATE Same as randsample, except allows for sampling of
%rows from a matrix
%   Inputs:
%       population - [n x d] sample matrix
%       k - number of rows to sample from population (without replacement)
%
%   Outputs:
%       Y - [k x d] samples

[~, n] = size(population);
i = randsample(1:n, k);
Y = population(:, i);

end

