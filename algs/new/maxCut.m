function [ labels, bestcut ] = maxCut( W )
%MAXCUT Randomized Max Cut algorithm due to Goemans and Williamson (1995)
%   Inputs:
%       W - Adjacency matrix
%
%   Outputs:
%       S - Vector of cluster labels {0, 1}^n

[n, ~] = size(W);
if n == 0
    error('W must have at least 2 entries')
elseif n == 1
    labels = 1;
    bestcut = 0;
    return
end
    
Wtot = sum(sum(W))/2;
Wneg = sum(sum(W.*(W < 0)))/2;
if ~all(diag(W) == zeros(n, 1))
    error('No self edges allowed. Set diagonal elements of adjacency matrix to zero')
elseif ~all(W == W')
    error('Graph must be undirected. Make sure adjacency matrix is symmetric')
end

%% CVX
cvx_begin sdp
    variable Y(n,n) symmetric
    maximize Wtot/2 - trace(W*Y)/4
    subject to
        Y >= 0;
        diag(Y) == ones(n,1);
cvx_end
Z_Pstar = Wtot/2 - trace(W*Y)/4;
disp(['Semidefinite bound on max cut: ', num2str(Z_Pstar)])
[V, p] = chol(Y); % May fail occasionally due to numerical inaccuracy
if p
    labels = ones(n, 1);
    warning('Optimization result is not positive semi-definite');
    bestcut = 0;
    return
end
    


%% Random projection
p = 50; % number of random projections to try, pick the best
labels = sign(V'*randn(n,p));  % each column of sigma is a labeling
D = diag(sum(W, 2));
L = D - W;  % Graph Laplacian
Z_cut = diag(labels'*L*labels)/4;
disp(['Best max cut ', num2str(max(Z_cut))])
disp(['Mean cut to semidefinite bound ratio ', num2str((mean(Z_cut)-Wneg)/(Z_Pstar-Wneg))])
[bestcut, ibest] = max(Z_cut);
disp(['Best cut to semidefinite bound ratio ', num2str((bestcut-Wneg)/(Z_Pstar-Wneg))])
labels = labels(:, ibest);

end

