function [ labels ] = maximizeAgreements( W_neg, W_pos, varargin )
%MAXIMIZEAGREEMENTS Correlation clustering, maximizing agreements
%   Inputs:
%       W_pos -- reward adjacency matrix of labeling an edge positive
%       W_neg -- reward adjacency matrix of labeling an edge positive
%
%   Outputs:
%       labels -- clusters labels. [n x 1] column vector, labels are zero
%       indexed

%% Input checking and parsing
validateattributes(W_neg, {'numeric'}, {'nonpositive', 'square'})
validateattributes(W_pos, {'numeric'}, {'nonpositive', 'square'})
if any(diag(W_neg) ~= 1)
    error('Self edges should not impact likelihood')
elseif any(diag(W_pos) ~= 1)
    error('Self edges should not impact likelihood')
elseif length(W_neg) ~= length(W_pos)
    error('Matrix dimensions must match')
else
    n = length(W_pos);
end

p = inputParser;
defaultApproximation = 'Swamy75';
expectedApproximations = {'Swamy75'};  % can eventually add Charikar and other Swamy approximations
addOptional(p,'approximation',defaultApproximation,...
            @(x) any(validatestring(x,expectedApproximations)));
parse(p,varargin{:});
approximation = p.Results.approximation;


%% Swamy 0.75-approximation
if strcmp(approximation, 'Swamy75')
    % CVX (SDPT-3)
    W = W_pos - W_neg;
    cvx_begin sdp
        variable U(n,n) symmetric
        maximize trace(U*W)
        subject to
            U >= 0;
            diag(U) == ones(n,1);
    cvx_end
    [V, p] = chol(U); % May fail occasionally due to numerical inaccuracy
    if p
        V = NaN;
        warning('Optimization result is not positive semi-definite');
        return
    end

    % Random projections
    labels = bi2de((V'*randn(n,2)) > 0);
    U_round = ~squareform(pdist(labels, 'hamming'));

end

end

