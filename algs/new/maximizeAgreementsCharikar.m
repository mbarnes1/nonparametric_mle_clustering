function [ labels ] = maximizeAgreementsCharikar( E, C, varargin )
%MAXIMIZEAGREEMENTSCHARIKAR Correlation clustering, maximizing agreements
%   Inputs:
%       E -- edge binary adjacency matrix {0, 1}^[n x n]. 1=edge, 0 =
%       non-edge
%       C -- mistake cost adjacency matrix [n x n]. C \geq 0, C(i, i) = 0
%
%   Outputs:
%       labels -- clusters labels. [n x 1] column vector, labels are zero
%       indexed

%% Input checking and parsing
validateattributes(E, {'logical'}, {'square'})
validateattributes(C, {'numeric'}, {'nonnegative', 'square'})
if any(diag(C) ~= 0)
    error('Self edges should not have cost')
elseif length(E) ~= length(C)
    error('Matrix dimensions must match')
else
    n = length(E);
end

p = inputParser;
defaultApproximation = 'Charikar';
expectedApproximations = {'Charikar'};  % can eventually add Swamy
addOptional(p,'approximation',defaultApproximation,...
            @(x) any(validatestring(x,expectedApproximations)));
parse(p,varargin{:});
approximation = p.Results.approximation;


%% Charikar 0.7664-approximation
if strcmp(approximation, 'Charikar')
    % CVX (SDPT-3)
    W = (2*E-1).*C;
    cvx_begin sdp
        variable U(n,n) symmetric
        maximize trace(U*W)
        subject to
            U >= 0;
            diag(U) == ones(n,1);
    cvx_end
    [V, p] = chol(U); % May fail occasionally due to numerical inaccuracy
    if p
        V = NaN;
        warning('Optimization result is not positive semi-definite');
        return
    end

    % Random projections
    labels = (V'*randn(n,3)) > 0;
    labels4 = bi2de(labels(:, 1:2));
    labels8 = bi2de(labels(:, 1:3));
    % Pick best of two projections
    U4 = ~squareform(pdist(labels4, 'hamming'));
    U8 = ~squareform(pdist(labels8, 'hamming'));
    [~, iMax] = max([trace(U4*W), trace(U8*W)]);
    switch iMax
        case 1
            labels = labels4;
            U_round = U4;
        case 2
            labels = labels8;
            U_round = U8;
    end
    
    %% Approximation ratio result
    disp(['Rounded solution is at least a ', ...
        num2str((2*trace(U_round*W)-sum(sum(W)))/(2*trace(U*W)-sum(sum(W)))), '-approximation']);
end

end

