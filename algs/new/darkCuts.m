function [ labels ] = darkCuts( X, P0, P1, varargin )
%DARKCUTS Clusters based on maximum likelihood estimator
%   Inputs:
%       X - [n*(n-1)/2, d] d-dimensional edge features to cluster. The rows
%       of X are ordered according to squareform()
%       P0 - a pretrained non-edge feature pdf
%       P1 - a pretrained edge feature pdf
%       varargin options:
%           'Method' -- 'TwoCluster', 'MinimizeDisagreements', or
%           'MaximizeAgreements'
%                        TwoCluster = Two clusters, solve with Goemanns and
%                               Williamson
%                        MinimizeDisagreements = Solve with Demaine et al.
%                        MaximizeAgreements = Solve with Swamy et al.
%
%   Outputs:
%       labels -- clusters labels. [n x 1] column vector, labels are one
%       indexed


%% Parse Inputs
validateattributes(P0, {'kde'}, {})
validateattributes(P1, {'kde'}, {})
validateattributes(X, {'numeric'}, {'nonnan'});
p = inputParser;
defaultMethod = 'MinimizeDisagreements';
expectedMethods = {'MinimizeDisagreements', 'MaximizeAgreements', 'TwoCluster'};
addParameter(p,'Method',defaultMethod,...
                 @(x) any(validatestring(x,expectedMethods)));
parse(p, varargin{:});
method = p.Results.Method;


%% Cluster
if strcmp(method, 'MaximizeAgreements')
    W_neg = squareform(log(evaluate(P0,X)));
    W_pos = quareform(log(evaluate(P1,X)));
    W_neg(logical(eye(size(W_neg)))) = 1;
    W_pos(logical(eye(size(W_pos)))) = 1;
    labels = maximizeAgreements(W_neg, W_pos);
elseif strcmp(method, 'MinimizeDisagreements')
    log_odds = squareform(log(evaluate(P1,X)) - log(evaluate(P0,X)));
    E = logical(log_odds>0);
    C = abs(log_odds);
    C(logical(eye(size(C)))) = 0;
    labels = minimizeDisagreements(E, C, 'Solver', 'Cplex');
elseif strcmp(method, 'TwoCluster')
    log_odds = squareform(log(evaluate(P1,X)) - log(evaluate(P0,X)));
    labels = maxCut(-log_odds);
else
    error('Invalid method')
end
validateattributes(labels, {'numeric'}, {'vector', 'integer', 'positive'})

end

