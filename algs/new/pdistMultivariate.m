function [ D ] = pdistMultivariate( X )
%PDISTMULTIVARIATE Pairwise distances, allowing for vector distances
%   Inputs:
%       X - [n x d] matrix of samples
%
%   Outputs:
%       D - [f x (n*(n-1)/2)] matrix of distances. f is the dimensional 
%           output of the distance function
validateattributes(X, {'numeric'}, {'nonnan'})

[n, d] = size(X);
D = NaN(d, n*(n-1)/2);
for i = 1:d
    D(i, :) = pdist(X(:, i));
end

end

