function cmap = colorBlind(k)

colors = [51 34 136;
    102 153 204;
    136 204 238;
    68 170 153;
    17 119 51;
    153 153 51;
    221 204 119;
    102 17 0;
    204 102 119;
    170 68 102;
    136 34 85;
    170 68 153];
if k == 1
    cmap = [68 119 170];
elseif k == 2
    cmap = [68 119 170;
        170 68 153];
elseif k == 3
    cmap = [68 119 170;
        17 119 51;
        170 68 153];
elseif k == 4
    cmap = [68 119 170;
        17 119 51;
        221 204 119;
        170 68 153];
elseif k == 5
    cmap = [51 34 136;
        136 204 238;
        17 119 51;
        221 204 119;
        170 68 153];
elseif k == 6
    ids = [1 3 5 7 9 12];
    cmap = colors(ids, :);
elseif k == 7
    ids = [1 3 4 5 7 9 12];
    cmap = colors(ids, :);
elseif k == 8
    ids = [1 3 4 5 6 7 9 12];
    cmap = colors(ids, :);
elseif k == 9
    ids = [1 3 4 5 6 7 9 11 12];
    cmap = colors(ids, :);
elseif k == 10
    ids = [1 3 4 5 6 7 8 9 11 12];
    cmap = colors(ids, :);
elseif k == 11
    ids = [1 2 3 4 5 6 7 8 9 11 12];
    cmap = colors(ids, :);
elseif k == 12
    cmap = colors;
else
    error('More clusters than colors!')
end
cmap = cmap/256;
cmap = rgb2hsv(cmap);
cmap(:, 3) = cmap(:, 3)*1.4;  % increase brightness
cmap(:, 2) = cmap(:, 2)*1.3;  % increase saturation
cmap = min(cmap, 1);
cmap = hsv2rgb(cmap);

end