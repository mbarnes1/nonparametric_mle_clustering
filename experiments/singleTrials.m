clear, clc, close all
addpath(genpath(fileparts(pwd)))
if ~exist('kde', 'file')
    error('Please install the KDE toolbox from http://www.ics.uci.edu/~ihler/code/kde.html')
end

%% Datasets
dataset_names = {'Skin'};
n = 100;
datasets = getDatasets(n+2000, dataset_names);
datasets_train = getDatasets(2000, dataset_names);
dataset_names = fieldnames(datasets);
knn = 100;  % for spectral similarity graph

for iData = 1:length(dataset_names)
    X = datasets.(dataset_names{iData}).data;
    Y = datasets.(dataset_names{iData}).labels;
    X_train = X((n+1):end, :);
    Y_train = Y((n+1):end, :);
    X = X(1:n, :);
    Y = Y(1:n, :);
    k = length(unique(Y));
    if size(X, 2) == 2 || size(X,2) == 3
        plotClusters(X, Y, Y, dataset_names{iData});
    end
    
    %% Dark cuts
    F = pdistMultivariate(X);
    F_train = pdistMultivariate(X_train);
    A_train = ~pdist(Y_train, 'hamming');
    
    P0 = kde(randsampleMultivariate(F_train(:, A_train==0), min(5000, sum(A_train==0))), 'rot');
    P1 = kde(randsampleMultivariate(F_train(:, A_train==1), min(5000, sum(A_train==1))), 'rot');
    d = size(F, 1);  % dimensionality of pairwise features
    if d == 1
        figure; hold on;
        cmap = colorBlind(2);
        i = linspace(min(F_train), max(F_train), 1000);
        plot(i, evaluate(P0, i), 'Color', cmap(1, :), 'LineWidth', 3);
        plot(i, evaluate(P1, i), 'Color', cmap(2, :), 'LineWidth', 3);
        xlim([min(F_train), max(F_train)])
        xlabel('Feature f'); ylabel('P(f | edge)'); legend('Non-edge', 'Edge');
    elseif d == 2
        figure;
        cmap = colorBlind(2);
        [iX, iY] = meshgrid(linspace(min(F_train(1, :)), max(F_train(1, :)), 100), linspace(min(F_train(2, :)), max(F_train(2, :)), 100));
        p = evaluate(P0, [iX(:), iY(:)]');
        p = reshape(p, size(iX));
        surf(iX, iY, p)
        xlabel('Edge Feature 1'); ylabel('Edge Feature 2'); zlabel('P_0(e)');
        view([-162.5, 46])
        figure
        p = evaluate(P1, [iX(:), iY(:)]');
        p = reshape(p, size(iX));
        surf(iX, iY, p)
        xlabel('Edge Feature 1'); ylabel('Edge Feature 2'); zlabel('P_1(e)');
        view([-162.5, 46])
    end

    Y_dark = darkCuts(F, P0, P1, 'Method', 'MinimizeDisagreements');
    if size(X, 2)  == 2 || size(X,2) == 3
        plotClusters(X, Y_dark, Y, [dataset_names{iData}, ' with dark cuts'])
    end
    metrics = evaluateClusters(Y_dark, Y);
    metrics.NMI
    

    %% k-means
    Y_kmeans = kmeans(X, k);
    if size(X, 2) == 2 || size(X,2) == 3
        plotClusters(X, Y_kmeans, Y, [dataset_names{iData}, ' with k-means'])
    end
    metrics = evaluateClusters(Y_kmeans, Y);
    metrics.NMI

    %% Spectral clustering
    X_whitened = bsxfun(@plus, X, - mean(X))/mean(var(X));
    S = SimGraph_NearestNeighbors(X_whitened, knn, 1);  % 1 = normal, 2 = mutual
    Y_spectral = SpectralClustering(S, k, 1);  % 1 = unnormalized, 2 = normalized
    if size(X, 2) == 2 || size(X,2) == 3
        plotClusters(X, Y_spectral, Y, [dataset_names{iData}, ' with normalized spectral'])
    end
    metrics = evaluateClusters(Y_spectral, Y);
    metrics.NMI
    
    %idx_spectral_unnormalized = SpectralClustering(S, k, 1);
    %plotClusters(X, idx_spectral_unnormalized, Y, [dataset_names{iData}, ' with unnormalized spectral'])
end

   