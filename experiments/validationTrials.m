clear, clc, close all
addpath(genpath(fileparts(pwd)))
if ~exist('kde', 'file')
    error('Please install the KDE toolbox from http://www.ics.uci.edu/~ihler/code/kde.html')
end

%% Datasets
dataset_names = {'Motor'};
n = 60;
ntrials = 10;
dim_red = [1 2 3 5 10 20];
datasets = getDatasets(10*n*ntrials, dataset_names);
dataset_names = fieldnames(datasets);
knn = 20;  % for spectral similarity graph
results_mle_euclidean = NaN(ntrials, length(dim_red));
results_mle_reduced = NaN(ntrials, length(dim_red));
results_mle_full = NaN(ntrials, length(dim_red));
results_spectral = NaN(ntrials, length(dim_red));
results_kmeans = NaN(ntrials, length(dim_red));

for iData = 1:length(dataset_names)
    data = datasets.(dataset_names{iData}).data;
    labels = datasets.(dataset_names{iData}).labels;
    validate_indices = crossvalind('Kfold', size(data, 1), ntrials*10);
    for iDim = 1:length(dim_red)
        [~, data_reduced] = pcares(zscore(data), dim_red(iDim));
        data_reduced = data_reduced(:, 1:dim_red(iDim));
        results_mle_euclidean = NaN(1, ntrials);
        for iTrial = 1:ntrials
            X = data(validate_indices==iTrial, :);
            X_reduced = data_reduced(validate_indices==iTrial, :);
            Y = labels(validate_indices==iTrial, :);
            iTrain = false(length(validate_indices));
            for i = 1:9
                iTrain(validate_indices==(iTrial + i*ntrials)) = 1;
            end
            X_train = data(iTrain, :);
            X_train_reduced = data_reduced(iTrain, :);
            Y_train = labels(iTrain, :);
            k = length(unique(Y));

            %% Dark cuts
            F = pdistMultivariate(X_reduced);
            F_train = pdistMultivariate(X_train_reduced);
            A_train = ~pdist(Y_train, 'hamming');

            P0 = kde(randsampleMultivariate(F_train(:, A_train==0), min(5000, sum(A_train==0))), 'rot');
            P1 = kde(randsampleMultivariate(F_train(:, A_train==1), min(5000, sum(A_train==1))), 'rot');
            Y_dark = darkCuts(F, P0, P1, 'Method', 'MinimizeDisagreements');
            metrics = evaluateClusters(Y_dark, Y);
            results_mle_reduced(iTrial, iDim) = metrics.NMI;

            % Euclidean
            F = pdist(X);
            F_train = pdist(X_train);
            A_train = ~pdist(Y_train, 'hamming');

            P0 = kde(randsampleMultivariate(F_train(:, A_train==0), min(5000, sum(A_train==0))), 'rot');
            P1 = kde(randsampleMultivariate(F_train(:, A_train==1), min(5000, sum(A_train==1))), 'rot');
            Y_dark = darkCuts(F, P0, P1, 'Method', 'MinimizeDisagreements');
            metrics = evaluateClusters(Y_dark, Y);
            results_mle_euclidean(iTrial, iDim) = metrics.NMI;
            
            % Full space
            F = pdistMultivariate(X); 
            F_train = pdistMultivariate(X_train);
            A_train = ~pdist(Y_train, 'hamming');

            P0 = kde(randsampleMultivariate(F_train(:, A_train==0), min(5000, sum(A_train==0))), 'rot');
            P1 = kde(randsampleMultivariate(F_train(:, A_train==1), min(5000, sum(A_train==1))), 'rot');
            Y_dark = darkCuts(F, P0, P1, 'Method', 'MinimizeDisagreements');
            metrics = evaluateClusters(Y_dark, Y);
            results_mle_full(iTrial, iDim) = metrics.NMI;
            
            %% k-means
            Y_kmeans = kmeans(X, k);
            metrics = evaluateClusters(Y_kmeans, Y);
            results_kmeans(iTrial, iDim) = metrics.NMI;


            %% Spectral clustering
            try
                %X_whitened = bsxfun(@plus, X, - mean(X));
                S = SimGraph_NearestNeighbors(X, knn, 2);  % 1 = normal, 2 = mutual
                Y_spectral = SpectralClustering(S, k, 2);  % 1 = unnormalized, 2 = normalized
                metrics = evaluateClusters(Y_spectral, Y);
                results_spectral(iTrial, iDim) = metrics.NMI;
            catch
                results_spectral(iTrial, iDim) = NaN;
            end

            %idx_spectral_unnormalized = SpectralClustering(S, k, 1);
            %plotClusters(X, idx_spectral_unnormalized, Y, [dataset_names{iData}, ' with unnormalized spectral'])
        end
    end
end

   