function [ metrics ] = evaluateClusters( predicted, truth )
%EVALUATECLUSTERS Evaluates the predicted clustering labels using several
%performance metrics
%   Inputs:
%       predicted - [n x 1] vector of predicted sample labels
%       truth - [n x 1] vector of true sample labels
%
%   Outputs:
%       metrics - object with fields for every performance metric

R = clusterLabels(predicted);
S = clusterLabels(truth);
metrics = struct();

%% Normalized mutual information
metrics.NMI = normalizedMutualInformation(R, S);

%% Pairwise metrics
[precision, recall] = pairwise_precision_recall(R, S);
f1 = 2*precision*recall/(precision+recall);
metrics.precision = precision;
metrics.recall = recall;
metrics.f1 = f1;

end

function [precision, recall] = pairwise_precision_recall(R, S)
%PAIRWISE_PRECISION_RECALL As defined by Menestrina et al. 2010
pairsR = cellfun(@(x) combnk(x, 2), R, 'UniformOutput', false);
pairsR = vertcat(pairsR{:});
pairsS = cellfun(@(x) combnk(x, 2), S, 'UniformOutput', false);
pairsS = vertcat(pairsS{:});
intersection_size = size(intersect(pairsR,pairsS,'rows'), 1);
precision = mydivide(intersection_size, size(pairsR, 1));
recall = mydivide(intersection_size, size(pairsS, 1));
end

function [ NMI ] = normalizedMutualInformation(R, S)
%NORMALIZEDMUTUALINFORMATION As defined by Stanford NLP
NMI = 2* mutualInformation(R, S)/(entropy(R) + entropy(S));
end

function [ I ] = mutualInformation(R, S)
%MUTUALINFORMATION
%   Inputs:
%       R - predicted clusters (kx1 cell array of clusters. Each cluster is
%       a vector of sample ids)
%       S - true clusters (same format)
%
%   Outputs:
%       I - the mutual information
n = sum(cellfun(@numel, R));
mi = @(c1, c2) mymult(length(intersect(c1, c2))/n, log(n*length(intersect(c1, c2))/(length(c1)*length(c2))));
miS = @(c1) sum(cellfun(@(x) mi(x,c1), R));
I = sum(cellfun(miS, S));
end

function y = mymult(a, b)
%MYMULT Conventionaly multiplication, with modification 0*Inf=0
y = a*b;
if ~isnan(a) && ~isnan(b) && isnan(y)
    y = 0;
end
end

function y = mydivide(a, b)
%MYDIVIDE Conventional division, with modification 0/0 = 0
if a == 0 && b == 0
    y = 0;
else
    y = a/b;
end
end

function [ H ] = entropy(R)
%ENTROPY
%   Inputs:
%       R - clusters (kx1 cell array of clusters. Each cluster is
%       a vector of sample ids)
%
%   Outputs:
%       H - the entropy
n = sum(cellfun(@numel, R));
singleEntropy = @(c) -length(c)/n*log(length(c)/n);
H = sum(cellfun(singleEntropy, R));
end

function [ clusters ] = clusterLabels(labels)
%CLUSTER Put labels into clusters
%   Inputs:
%       labels - [n x 1] vector of sample labels
%
%   Outputs:
%       clusters - (kx1 cell array of clusters. Each cluster is
%       a vector of sample ids)

unique_labels = unique(labels);
ids = 1:length(labels);
clusters = cell(length(unique_labels), 1);
for i = 1:length(unique_labels)
    label = unique_labels(i);
    clusters{i} = ids(labels == label);
end

end