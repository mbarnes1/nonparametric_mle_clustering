function [ ] = plotClusters( data, labels, true_labels, name, varargin )
%PLOTRESULTS Common plotting scheme
%   Inputs:
%       data - [n x 2] or [n x 3] array of samples
%       labels - [n x 1] vector of labels
%       true_labels = [n x 1] vector of true labels (for matching colors)
%       name - String of dataset name
%       varargin - k, the number of colors to use

if ~isempty(varargin)
    k = varargin{1};
    validateattributes(k, {'numeric'}, {'scalar', 'integer', 'positive'});
else
    k = max(length(unique(labels)), length(unique(true_labels)));
end
d = size(data, 2);

f = figure;
colormap(colorBlind(k));  % jet
true_labels = remapLabels(true_labels);
labels = matchLabels(labels, true_labels);
if d == 2
    scatter(data(:, 1), data(:, 2), 70, labels, 'filled');
    %ylim([0, 2])
    axis square
    set(gcf, 'Position', [0 0 300 300])
elseif d == 3
    h = scatter3(data(:, 1), data(:, 2), data(:, 3), 70, labels, 'filled');
    set(gcf, 'Position', [0 0 400 300])
    view([-106.5, 22])
else
    error('Can only plot 2d or 3d data');
end

%title(name);
%saveas(f, ['figures/', name, '.fig']);
%saveas(f, ['figures/', name, '.eps'], 'epsc');

end

function new_labels = matchLabels(predicted_labels, true_labels)
%MATCHLABELS Matches the labels to true labels as best possible (so colors
%correspond nicely in plots)
%TODO: When number of clusters and predicted is not the same
predicted_labels = predicted_labels + pi;  % plus pi so very unique 
unique_predicted = unique(predicted_labels);
unique_true = unique(true_labels);
permutations = permsk(unique_predicted, length(unique_true));
best_acc = 0;
best_perm = NaN;
for i = 1:size(permutations, 1)
    perm = permutations(i, :);
    new_labels = predicted_labels;
    for j = 1:length(perm)
        new_labels(new_labels == perm(j)) = unique_true(j);
    end
    acc = sum(new_labels == true_labels)/length(true_labels);
    if acc > best_acc
        best_acc = acc;
        best_perm = perm;
    end
end
new_labels = predicted_labels;
for i = 1:length(best_perm)
	new_labels(new_labels == best_perm(i)) = unique_true(i);
end
% Take care of extra predicted labels
extra_counter = 1;
for i = 1:length(unique_predicted)
    if any(unique_predicted(i)==new_labels)
        new_labels(new_labels==unique_predicted(i)) = max(unique_true) + extra_counter;
        extra_counter = extra_counter+1;
    end
end
validateattributes(new_labels, {'numeric'}, {'vector', 'integer', 'positive'});
end


function permutations = permsk(v, k)
%PERMSK All permutations of length k from v
C = combnk(v, k);
permutations = cell(size(C, 1) , 1);
for iC = 1:size(C, 1)
    permutations{iC} = perms(C(iC, :));
end
permutations = vertcat(permutations{:});
end


function new_labels = remapLabels(labels)
%REMAPLABELS Maps labels to integers 1, 2, ....
unique_labels = unique(labels);
new_labels = zeros(size(labels));
for i = 1:length(unique_labels);
    new_labels(labels == unique_labels(i)) = i;
end
end
