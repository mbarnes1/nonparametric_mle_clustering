clear, clc, close all
addpath(genpath(pwd))

%% Datasets
datasets = getDatasets(400);
dataset_names = fieldnames(datasets);
ntrials = 1;
performance = cell(5, ntrials);
iData = 1;

for iTrial = 1:ntrials
    X = datasets.(dataset_names{iData}).data;
    idx = datasets.(dataset_names{iData}).labels;
    k = length(unique(idx));
    performance{5, iTrial} = datasets.(dataset_names{iData}).params;
    plotClusters(X, idx, idx, dataset_names{iData});

    %% k-means
    try
        display(['Running k-means on ', dataset_names{iData}, '...'])
        %tic
        idx_kmeans = kmeans(X, k);
        %times(1, iN, iR) = toc;
        performance{1, iTrial} = evaluateClusters(idx_kmeans, idx);
        plotClusters(X, idx_kmeans, idx, [dataset_names{iData}, ' with k-means'])
    catch
        display('There was an error')
    end

    %% Spectral clustering
    display(['Running spectral clustering on ', dataset_names{iData}, '...'])
    for knn = 6:6:48
        S = SimGraph_NearestNeighbors(X, knn, 2); %datasets.(dataset_names{iData}).params.knn, 2);
        try
            %tic
            idx_spectral_normalizedrw = SpectralClustering(S, k, 2);
            %times(2, iN, iR) = toc;
            performance{2, iTrial} = evaluateClusters(idx_spectral_normalizedrw, idx);
            plotClusters(X, idx_spectral_normalizedrw, idx, [dataset_names{iData}, ' with normalized spectral'])
        catch
            display('There was an error')
        end
        %tic
        S = SimGraph_NearestNeighbors(X, knn, 2);
        try
            idx_spectral_unnormalized = SpectralClustering(S, k, 1);
            plotClusters(X, idx_spectral_unnormalized, idx, [dataset_names{iData}, ' with unnormalized spectral'])
        catch
            display('There was an error')
        end
    end
    %times(3, iN, iR) = toc;
    performance{3, iTrial} = evaluateClusters(idx_spectral_unnormalized, idx);

    %% Dark cuts
    display(['Running dark cuts on ', dataset_names{iData}, '...'])
    for dark_distance = linspace(0.2, 1, 20)
        s = @(x1, x2) norm(x1-x2) < dark_distance; %datasets.(dataset_names{iData}).params.dark_distance;
        [tpr, fpr, tnr, fnr] = evaluateSimilarityFunction(s, X, idx);
        W = darkGraph(s, X, tpr, fpr);
        %tic
        try
            [idx_dark, cut] = maxCut(W); %iterativeMaxCut(W, datasets.(dataset_names{iData}).params.dark_stop);
        catch
            display('There was an error')
        end
        %times(4, iN, iR) = toc;
        performance{4, iTrial} = evaluateClusters(idx_dark, idx);
        plotClusters(X, idx_dark, idx, [dataset_names{iData}, ' with dark cuts'])
    end
end
   