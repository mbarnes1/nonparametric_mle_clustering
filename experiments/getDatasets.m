function [ dataset ] = getDatasets( n, varargin )
%GETDATASETS All datasets
%   Inputs:
%       n - number of samples. If too few real samples available, returns
%       as many as possible
%       (optional) datasets - cell of strings. Any specific dataset name,
%        and/or 'synthetic' and/or 'real' for all of those datasets
%   Outputs:
%       dataset - structure with fields for every dataset

%% Parse and check inputs
validSyntheticDatasets = {'Gaussian', 'UnevenGaussian', 'GaussianUniform', ...
    'Moon', 'Circle', 'SwissRoll', 'ICML', 'CrossBones', 'Grid'};
validRealDatasets = {'Ionosphere', 'Sat', 'Skin', 'Motor'};
validDatasets = [validSyntheticDatasets, validRealDatasets];

validateattributes(n, {'numeric'}, {'integer', 'positive'})
if isempty(varargin)
    requestedDatasets = {'Synthetic'};
else
    requestedDatasets = varargin{1};
    validateattributes(requestedDatasets, {'cell'}, {'nonempty'});
    for i = 1:length(requestedDatasets)
        validatestring(requestedDatasets{i},[validDatasets, {'Real', 'Synthetic'}]);
    end
end

datasets = {};
if ismember('Synthetic', requestedDatasets)
    datasets = [datasets, {'Gaussian', 'UnevenGaussian', ...
        'GaussianUniform', 'Moon', 'Circle', 'SwissRoll', 'ICML', ...
        'CrossBones', 'Grid'}];
end
if ismember('Real', requestedDatasets)
    datasets = [datasets, {'Ionosphere', 'Sat', 'Skin', 'Motor'}];
end
for i = 1:length(validDatasets)
    if ismember(validDatasets{i}, requestedDatasets) && ~ismember(validDatasets{i}, datasets)
        datasets = [datasets, validDatasets(i)];
    end
end

%% Build synthetic datasets
dataset = struct();

% Gaussian
if ismember('Gaussian', datasets)
    sig = 0.15*[1 0.5; 0.5 1];
    x1 = mvnrnd([-1; 1], sig, floor(n/3));
    y1 = 1*ones(floor(n/3), 1);
    x2 = mvnrnd([1; 1], sig, floor(n/3));
    y2 = 2*ones(floor(n/3), 1);
    x3 = mvnrnd([0; -1], sig, n - length(y1)-length(y2));
    y3 = 3*ones(n - length(y1)-length(y2), 1);
    data = [x1; x2; x3];
    labels = [y1; y2; y3];
    [data, labels] = shuffle(data, labels);
    dataset.Gaussian.data = data;
    dataset.Gaussian.labels = labels;
end

% Uneven Gaussian
if ismember('UnevenGaussian', datasets)
    sig = [1 0; 0 1];
    x1 = mvnrnd([-6, 0], 4*sig, floor(8*n/10));
    y1 = 1*ones(size(x1, 1), 1);
    x2 = mvnrnd([-0.5; 0], 0.25*sig, floor(n/10));
    y2 = 2*ones(size(x2, 1), 1);
    x3 = mvnrnd([2; 0], 0.25*sig, n - length(y1)-length(y2));
    y3 = 3*ones(n - length(y1)-length(y2), 1);
    data = [x1; x2; x3];
    labels = [y1; y2; y3];
    [data, labels] = shuffle(data, labels);
    dataset.UnevenGaussian.data = data;
    dataset.UnevenGaussian.labels = labels;
end

% Gaussian over Uniform
if ismember('GaussianUniform', datasets)
    sig = 0.1*[1 0; 0 1];
    x1 = 6*rand(floor(n/3), 2)-3;
    y1 = 1*ones(floor(n/3), 1);
    x2 = mvnrnd([0; 0.6], 0.3*sig, floor(n/3));
    y2 = 2*ones(floor(n/3), 1);
    x3 = mvnrnd([0; -0.6], 0.3*sig, n - length(y1)-length(y2));
    y3 = 3*ones(n - length(y1)-length(y2), 1);
    data = [x1; x2; x3];
    labels = [y1; y2; y3];
    [data, labels] = shuffle(data, labels);
    dataset.GaussianUniform.data = data;
    dataset.GaussianUniform.labels = labels;
end

% Two moons
if ismember('Moon', datasets)
    [data, labels] = dbmoon(n);
    [data, labels] = shuffle(data, labels);
    dataset.Moon.data = data;
    dataset.Moon.labels = labels;
end

%  Cross Bones
if ismember('CrossBones', datasets)
    sigma = 0.05;
    x1 = rand(floor(n/2), 1);
    y1 = x1 + sigma*randn(floor(n/2), 1);
    x2 = rand(ceil(n/2), 1);
    y2 = 1-x2 + sigma*randn(ceil(n/2), 1);
    labels = [ones(length(y1), 1); 2*ones(length(y2), 1)];
    data = [x1, y1;
            x2, y2];
    [data, labels] = shuffle(data, labels);
    dataset.CrossBones.data = data;
    dataset.CrossBones.labels = labels;
end

%  Grid
if ismember('Grid', datasets)
    sigma = 0.05;
    gridsize = 3;
    clustersize = floor(n/(gridsize*2));
    labels1 = [ones(clustersize, 1);
        2*ones(clustersize, 1);
        3*ones(clustersize, 1)];
    x1 = labels1 - 1 + sigma*randn(length(labels1), 1);
    y1 = (gridsize-1)*rand(length(labels1), 1);
    labels2 = labels1 + gridsize;
    y2 = labels1 - 1 + sigma*randn(length(labels2), 1);
    x2 = (gridsize-1)*rand(length(labels2), 1);
    data = [x1, y1;
            x2, y2];
    labels = [labels1; labels2];
    [data, labels] = shuffle(data, labels);
    dataset.Grid.data = data;
    dataset.Grid.labels = labels;
end

%  Concentric circles
if ismember('Circle', datasets)
    sigma = 0.1;
    t = 2*pi*rand(n, 1);
    labels = [ones(floor(n/2), 1); 2*ones(ceil(n/2), 1)];
    r = labels + sigma*randn(n, 1);
    x = r.*cos(t);
    y = r.*sin(t);
    data = [x, y];
    [data, labels] = shuffle(data, labels);
    dataset.Circle.data = data;
    dataset.Circle.labels = labels;
end

% Intertwined swiss rolls
if ismember('SwissRoll', datasets)
    r = linspace(0,1,floor(n/2))';
    t = (2.5*pi/2)*(0.5 + 2*r);
    x1 = t.*cos(t);
    y1 = t.*sin(t);
    x2 = t.*cos(t+pi);
    y2 = t.*sin(t+pi);
    x = [x1; x2];
    y = [y1; y2];
    mindim = min(max(y)-min(y), max(x)-min(x));
    x = x+0.05*randn(size(x))*sqrt(mindim);
    y = y+0.05*randn(size(y))*sqrt(mindim);
    data = 0.2*[x, y];
    labels = [-1*ones(size(x1)); ones(size(x2))];
    [data, labels] = shuffle(data, labels);
    dataset.SwissRoll.data = data;
    dataset.SwissRoll.labels = labels;
end

% ICML 2016
if ismember('ICML', datasets)
    data = load('icml.mat');
    dataset.ICML.data = [data.X, data.Y];
    dataset.ICML.labels = data.label;
end

%% Real Datasets
% Ionosphere
if ismember('Ionosphere', datasets)
    data = load('ionosphere.features');
    labels = load('ionosphere.labels');
    [data, labels] = shuffle(data, labels);
    data = data(1:min(size(data, 1), n), :);
    labels = labels(1:min(length(labels), n));
    dataset.Ionosphere.data = data;
    dataset.Ionosphere.labels = labels;
end

% Sat
if ismember('Sat', datasets)
    data = load('sat.features.mat');
    labels = load('sat.labels.mat');
    [data, labels] = shuffle(data, labels);
    data = data(1:min(size(data, 1), n), :);
    labels = labels(1:min(length(labels), n));
    dataset.Sat.data = data;
    dataset.Sat.labels = labels;
end

% Skin
if ismember('Skin', datasets)
    data = load('skin.features.mat');
    data = data.X;
    labels = load('skin.labels.mat');
    labels = labels.labels;
    [data, labels] = shuffle(data, labels);
    data = data(1:min(size(data, 1), n), :);
    labels = labels(1:min(length(labels), n));
    dataset.Skin.data = data;
    dataset.Skin.labels = labels;
end

% Motor
if ismember('Motor', datasets)
    data = load('motor.features.mat');
    data = data.data;
    labels = load('motor.labels.mat');
    labels = labels.labels;
    [data, labels] = shuffle(data, labels);
    data = data(1:min(size(data, 1), n), :);
    labels = labels(1:min(length(labels), n));
    dataset.Skin.data = data;
    dataset.Skin.labels = labels;
end

end

function [ anew, bnew ] = shuffle(a, b)
%SHUFFLE Simultaneously row shuffles a and b

shuffling = randperm(size(a, 1));
anew = a(shuffling, :);
bnew = b(shuffling, :);

end

function [data, labels] =dbmoon(N)
% Usage: data=dbmoon(N,d,r,w)
% doublemoon.m - genereate the double moon data set in Haykin's book titled
% "neural networks and learning machine" third edition 2009 Pearson
% Figure 1.8 pp. 61
% The data set contains two regions A and B representing 2 classes
% each region is a half ring with radius r = 10, width = 6, one is upper
% half and the other is lower half
% d: distance between the two regions
% will generate region A centered at (0, 0) and region B is a mirror image
% of region A (w.r.t. x axis) with a (r, d) shift of origin
% N: # of samples each class, default = 1000
% d: seperation of two class, negative value means overlapping (default=1)
% r: radius (default=10), w: width of ring (default=6)
% 
% (C) 2010 by Yu Hen Hu
% Created: Sept. 3, 2010

w=0.5;
r=1;
d=-0.2;


% generate region A:
% first generate a uniformly random distributed data points from (-r-w/2, 0)
% to (r+w/2, r+w/2)
N1=10*N;  % generate more points and select those meet criteria
w2=w/2; 
done=0; data=[]; tmp1=[];
while ~done, 
    tmp=[2*(r+w2)*(rand(N1,1)-0.5) (r+w2)*rand(N1,1)];
    % 3rd column of tmp is the magnitude of each data point
    tmp(:,3)=sqrt(tmp(:,1).*tmp(:,1)+tmp(:,2).*tmp(:,2)); 
    idx=find([tmp(:,3)>r-w2] & [tmp(:,3)<r+w2]);
    tmp1=[tmp1;tmp(idx,1:2)];
    if length(idx)>= N, 
        done=1;
    end
    % if not enough data point, generate more and test
end
% region A data and class label 0
% region B data is region A data flip y coordinate - d, and x coordinate +r
data=[tmp1(1:N,:);
    [tmp1(1:N,1)+r -tmp1(1:N,2)-d]];
labels = [-1*ones(N, 1);
          ones(N, 1)];

end

